### Back End Application for Bachelor's thesis:

**Android-based Fog Computing Gateway with Over-The-Air Programming for personalized Health Monitoring**

by Kristo Karp

---

> This service can be found running at http://ec2-13-53-188-126.eu-north-1.compute.amazonaws.com until 20th of June 2019.

---

To get access to the required config files that contain database secrets then please contact: `Kristo.Karp[at]gmail.com`

## System Requirements

-   node version 10+
-   npm version 6+

## Available Scripts

In the project directory, you can run the following commands to use the service:


### `npm install`

This will install all the required dependecies for the project and should be run after cloning the repository.

### `npm start`

Runs the app in background mode.<br>
This runs the service in local environment at port `80` and can be accessed at `http://localhost/`

### `npm run start:dev`

This will run the application in development mode and will use nodemon to instantly restart the server when any changes to the code have been made.
Service will be accessible at `http://localhost:5000/`

## API endpoints
---
### GET `/firebase/user`

Returns user record for given user id

PARAMS:
* `id` : user id

RETURNS:
* `Object containing all existing fields for the user`

---
### GET `/firebase/users`

Returns all existing user records.

PARAMS:
* `does not take input params`

RETURNS:
* `docs` : all existing user records in an array
---
### POST `/firebase/new`

Used by front end application to create new users, takes any fields as input and adds the values to the newly created user's record.

PARAMS
* `fields` : object of user's values example `fields:{First: 'Kristo'}`

RETURNS
* `id` : id of the created user
---
### POST `/firebase/update`

Used by front end application to update users, takes any fields as input and adds/updates the values in the user's record.

PARAMS
* `fields` : object of user's values example `fields:{First: 'Kristo'}`
* `id` : user id

RETURNS
* `status` : 'success' if request was successful
---
### POST `/firebase/uploadFile`

Used by front end application to upload new dex files and stores them in google cloud storage.

PARAMS:
* `file` : file object that is uploaded to google storage

RETURNS:
* `status` : 'success' if request was successful
* `url` : url to the uploaded file
---
### POST `/firebase/pushFile`

Used by front end application to create a cloud message that notifies an android app about a new dex file.

PARAMS:
 * `user` : user id

RETURNS:
* `status` : 'success' if request was successful
---
### GET `/firebase/hrData`

Used by front end application to get all records containing user's heart rate histories (ordered by ascending timestamp ).

PARAMS:
* `user` : user id

RETURNS:
* `docs` : all heart rate history records for given user
---

### POST `/firebase/hrData`

Used by Android Application to upload user's heart rate history.

PARAMS:
* `user` : user id
* `heartRates` : array of integers containing user's heart rates

RETURNS:
* `id` : record id for the created heart rate history
---

### POST `/firebase/notify`

Used by Android Application to change the user's status from critical to non critical and vice versa.

PARAMS:
* `user` : user id
* `critical` : 1 if status changed to critical, 0 if status changed to non critical

RETURNS:
* `status` : 'success' if updating was successful
---

> In case of any further questions please contact Kristo.Karp[at]gmail.com
