'use strict';

const { docExists, getDocument } = require('./helpers.js');
const firebase = require('firebase');
const firebaseAdmin = require('firebase-admin');
const serviceAccount = require('../../../FireBaseConfig');
const { Storage } = require('@google-cloud/storage');

const storage = new Storage({
	projectId: serviceAccount.project_id,
	keyFilename: './FireBaseConfig.json',
});
const bucket = storage.bucket(serviceAccount.storageBucket);

const db = firebase.firestore();
const users = db.collection('users');

const uploadFileToStorage = (bucket, file) => {
	return new Promise((resolve, reject) => {
		const newFileName = `${Date.now()}_${file.originalname}`;
		const fileUpload = bucket.file(`DexFiles/${newFileName}`);

		const blobStream = fileUpload.createWriteStream({
			metadata: {
				contentType: file.mimetype,
			},
		});

		blobStream.on('error', error => {
			console.log(error);

			reject('Something is wrong! Unable to upload at the moment.');
		});

		blobStream.on('finish', () => {
			const url = `https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`;
			resolve(url);
		});

		blobStream.end(file.buffer);
	});
};

const getErrorCode = response => {
	const results = response.results;
	let error = null;

	results.forEach(result => {
		if (result.error) {
			error = result.error.code;
			return error;
		}
	});
	return error;
};

const pushNewFileUrlToDevice = (token, url) => {
	const payload = {
		data: {
			url: url,
		},
	};

	const options = {
		priority: 'high',
	};

	return new Promise((resolve, reject) => {
		firebaseAdmin
			.messaging()
			.sendToDevice(token, payload, options)
			.then(response => {
				const error = getErrorCode(response);
				if (error) {
					reject(error);
				} else {
					resolve(response);
				}
			})
			.catch(error => {
				reject(getErrorCode(error));
			});
	});
};

module.exports.uploadFile = async function(req, res) {
	const file = req.file;
	if (file) {
		uploadFileToStorage(bucket, file)
			.then(url => {
				res.status(200).send({
					status: 'success',
					url: url,
				});
			})
			.catch(error => {
				res.status(500).send({
					error: error,
				});
			});
	} else {
		res.status(401).send({
			error: 'No file attached',
		});
	}
};

module.exports.sendNewDexFileToUser = async function(req, res) {
	const user = req.body.user;

	if (user && (await docExists(users, user))) {
		const userDoc = await getDocument(users, user);
		pushNewFileUrlToDevice(userDoc.data().token, userDoc.data().filepath)
			.then(resp => {
				res.status(200).send({
					status: 'success',
				});
			})
			.catch(err => {
				console.log(err);
				res.status(500).send({
					error: err,
				});
			});
	} else {
		res.status(500).send({
			error: 'No existing user id found',
		});
	}
};
