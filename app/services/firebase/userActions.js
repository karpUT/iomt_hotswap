'use strict';

const { getDocument, getDocuments } = require('./helpers.js');
const firebase = require('firebase');

const db = firebase.firestore();
const users = db.collection('users');

function extend(obj, src) {
	for (const key in src) {
		if (src.hasOwnProperty(key)) {
			obj[key] = src[key];
		}
	}
	return obj;
}

module.exports.createUser = async function createUser(req, res) {
	const fields = req.body.fields;
	users
		.add(fields)
		.then(docRef => {
			res.status(200).send({
				id: docRef.id,
			});
		})
		.catch(err => {
			res.status(401).send({
				error: err,
			});
		});
};

module.exports.updateUser = async function updateUser(req, res) {
	const fields = req.body.fields;
	const user = req.body.id;

	if (user && fields) {
		users
			.doc(user)
			.update(fields)
			.then(() => {
				res.status(200).send({
					status: 'success',
				});
			})
			.catch(err => {
				res.status(403).send({
					error: err,
				});
			});
	} else {
		res.status(404).send({
			error: 'id or fields not defined',
		});
	}
};

module.exports.getUserById = async function getUserById(req, res) {
	const id = req.query.id;
	if (id) {
		const user = await getDocument(users, id);
		res.status(200).send(user.data());
	} else {
		res.status(401).send({
			error: 'id not provided when requesting user data',
		});
	}
};

module.exports.getAllUsers = async function getAllUsers(req, res) {
	const docs = await getDocuments(users);
	const data = [];
	docs.forEach(doc => {
		data.push(extend({ id: doc.id }, doc.data()));
	});
	res.status(200).send({
		docs: data,
	});
};
