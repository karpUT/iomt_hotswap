'use strict';

const { docExists, sendSms } = require('./helpers.js');
const firebase = require('firebase');

const db = firebase.firestore();
const users = db.collection('users');
const hrData = db.collection('hrdata');

module.exports.notify = async function notify(req, res) {
	const { user, critical } = req.body;

	if (user && critical !== undefined && docExists(users, user)) {
		users
			.doc(user)
			.update({
				hrCritical: critical,
			})
			.then(() => {
				res.status(200).send({
					status: 'success',
				});
			})
			.catch(err => {
				res.status(500).send({
					error: err,
				});
			});
	} else {
		res.status(403).send({ error: 'Invalid input data' });
	}
};

module.exports.uploadData = async function uploadData(req, res) {
	const { user, heartRates } = req.body;

	if (user && heartRates && docExists(users, user)) {
		hrData
			.add({
				heartRates: heartRates,
				user: user,
				timestamp: new Date().getTime(),
			})
			.then(docRef => {
				res.status(200).send({
					id: docRef.id,
				});
			})
			.catch(err => {
				res.status(500).send({
					error: err,
				});
			});
	} else {
		res.status(404).send({ error: 'Invalid input data' });
	}
};

module.exports.getHrData = async function getHrData(req, res) {
	const { user } = req.query;

	if (user && docExists(users, user)) {
		hrData
			.orderBy('timestamp', 'asc')
			.where('user', '==', user)
			.get()
			.then(docs => {
				const result = [];
				docs.forEach(doc => {
					result.push(doc.data());
				});
				res.status(200).send({
					docs: result,
				});
			})
			.catch(err => {
				console.log(err);

				res.status(500).send({
					error: err,
				});
			});
	} else {
		res.status(404).send({
			error: 'Valid user not provided',
		});
	}
};
