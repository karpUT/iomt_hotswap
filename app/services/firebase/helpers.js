module.exports.docExists = async function(collection, ref) {
	const docRef = collection.doc(ref);
	return await docRef
		.get()
		.then(doc => {
			return doc.exists;
		})
		.catch(err => {
			console.log(err);
			return false;
		});
};

module.exports.getDocument = async function(collection, ref) {
	const docRef = collection.doc(ref);
	return await docRef
		.get()
		.then(doc => {
			return doc;
		})
		.catch(err => {
			console.log(err);
			return false;
		});
};

module.exports.getDocuments = async function(collection) {
	return await collection
		.get()
		.then(docs => {
			return docs;
		})
		.catch(err => {
			return err;
		});
};

