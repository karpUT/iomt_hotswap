'use strict';

const express = require('express');
const firebaseController = require('../../../controllers/apis/firebase');

const router = express.Router();

router.use('/', firebaseController);

module.exports = router;
