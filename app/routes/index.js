'use strict';

const firebaseRoute = require('./apis/firebase');

function init(server) {
	server.get('*', (req, res, next) => {
		console.log(`Request was made to: ${req.originalUrl}`);
		return next();
	});

	server.use('/firebase', firebaseRoute);
}

module.exports = {
	init: init,
};
