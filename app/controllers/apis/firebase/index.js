'use strict';

const express = require('express');

const { createUser, updateUser, getUserById, getAllUsers } = require('../../../services/firebase/userActions');
const { uploadFile, sendNewDexFileToUser } = require('../../../services/firebase/fileActions');
const { notify, uploadData, getHrData } = require('../../../services/firebase/hrActions');
const multer = require('multer');

const router = express.Router();
const upload = multer({ storage: multer.memoryStorage() });

router.get('/user', getUserById);
router.get('/users', getAllUsers);
router.post('/new', createUser);
router.post('/update', updateUser);

router.post('/uploadFile', upload.single('File'), uploadFile);
router.post('/pushFile', sendNewDexFileToUser);

router.get('/hrData', getHrData);
router.post('/notify', notify);
router.post('/hrData', uploadData);

module.exports = router;
