'use strict';

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const firebase = require('firebase');
const firebaseAdmin = require('firebase-admin');
const serviceAccount = require('../FireBaseConfig');

firebase.initializeApp({
	projectId: serviceAccount.project_id,
	keyFilename: '../../../FireBaseConfig.json',
});

firebaseAdmin.initializeApp({
	credential: firebaseAdmin.credential.cert(serviceAccount),
	databaseURL: serviceAccount.database,
});

const app = express();
const port = process.env.PORT || 5000;
app.use(bodyParser.json());
app.use(cors());
const routes = require('./routes');
routes.init(app);

app.listen(port, () => {
	console.log(`Node started at port ${port}`);
});
